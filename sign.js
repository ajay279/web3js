var Tx     = require('ethereumjs-tx');
const Web3 = require('web3');
const provider = new Web3.providers.HttpProvider("https://ropsten.infura.io/v3/<API key>");
const web3 = new Web3(provider);
const account1 = '<your Account address>'; // Your account address 1
//const account2 = '' // Your account address 2
web3.eth.defaultAccount= account1;
const privateKey1 = Buffer.from('<Your Private key>', 'hex');
//const privateKey2 = Buffer.from('YOUR_PRIVATE_KEY_2', 'hex')
const abi=[{"constant":false,"inputs":[{"name":"_greeting","type":"string"}],"name":"greet","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"getGreeting","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"}];
const contract_Address="0xcbe74e21b070a979b9d6426b11e876d4cb618daf";
const contract =web3.eth.contract(abi).at(contract_Address);
let myData=contract.greet({from:account1},"hello blockchain devs").encodeABI();
web3.eth.getTransactionCount(account1, (err, txCount) => {
  // Build the transaction
  const txObject = {
    nonce:    web3.utils.toHex(txCount),
    to:       contract_Address,
    value:    web3.utils.toHex(web3.utils.toWei('0', 'ether')),
    gasLimit: web3.utils.toHex(21000),
    gasPrice: web3.utils.toHex(web3.utils.toWei('6', 'gwei')),
    data: myData  
  }

  // Sign the transaction
  const tx = new Tx(txObject);
  tx.sign(privateKey1);

  const serializedTx = tx.serialize();
  const raw = '0x' + serializedTx.toString('hex');

  // Broadcast the transaction
  web3.eth.sendRawTransaction(raw, (err, txHash) => {
    console.log('txHash:', txHash)
    // Now go check etherscan to see the transaction!
  })
})